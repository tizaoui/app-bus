package mybus.mybus.repository;

import mybus.mybus.domain.Fichier;
import mybus.mybus.domain.New;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewRepository extends JpaRepository<New, Integer> {
}
