package mybus.mybus.repository;

import mybus.mybus.domain.Reservation;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository  extends CrudRepository<Reservation,Integer> {

}
