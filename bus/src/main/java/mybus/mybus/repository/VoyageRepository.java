package mybus.mybus.repository;

import mybus.mybus.domain.Voyage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface VoyageRepository extends CrudRepository<Voyage,Integer> {
    public List<Voyage> findByDateAndLieu1AndLieu2(LocalDate date,String lieu1,String lieu2);
}
