package mybus.mybus.controller;

import mybus.mybus.domain.Comment;
import mybus.mybus.domain.User;
import mybus.mybus.service.CommentService;
import mybus.mybus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class CommentController {
@Autowired
    UserService userService;

    @Autowired
    CommentService commentService;
    @PostMapping("/comment")
   public Comment addComment(@RequestBody Comment comment)
    {

        return  commentService.addComment(comment);
    }

}
