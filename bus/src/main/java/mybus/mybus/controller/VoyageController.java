package mybus.mybus.controller;

import mybus.mybus.domain.Voyage;
import mybus.mybus.service.VoyageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController()
@RequestMapping("/api")
public class VoyageController {
    @Autowired
    VoyageService voyageService;
    @GetMapping("/voyage")
    public List<Voyage> findVoyages(@RequestParam(value="date")String date,@RequestParam(value="lieu1") String lieu1, @RequestParam(value="lieu2") String lieu2)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");



        //convert String to LocalDate
        LocalDate localDate = LocalDate.parse(date, formatter);

      return  voyageService.findVoyages(localDate,lieu1,lieu2);

    }}
