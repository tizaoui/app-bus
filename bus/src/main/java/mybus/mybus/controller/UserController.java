package mybus.mybus.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import mybus.mybus.domain.User;
import mybus.mybus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@RestController()
@RequestMapping("/api")
public class UserController {
@Autowired
    UserService userService;
    @PostMapping("/addUser")
  public User addUser(@RequestBody User user)
    {


       return userService.addUser(user);

}
    @GetMapping("/user")
    public  User user(Principal user) {
        return userService.findByUserName(user.getName());
    }



}
