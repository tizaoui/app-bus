package mybus.mybus.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import mybus.mybus.domain.New;
import mybus.mybus.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NewController {
    @Autowired
    NewService newService;

    @PostMapping("/new")
    public New addNew(@RequestPart("new1") String adString, @RequestPart("file") MultipartFile file) throws IOException
    {
       New new1 = new ObjectMapper().readValue(adString,New.class);

        return newService.addNew(new1,file);



    }
    @GetMapping("/new/{id}")
    public New getById(@PathVariable Integer id)
    {


        return newService.getById(id);



    }
    @GetMapping("/new")
    public List<New> getAll()
    {


        return newService.getAll();



    }
}
