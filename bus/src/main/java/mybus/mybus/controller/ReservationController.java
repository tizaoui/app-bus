package mybus.mybus.controller;

import mybus.mybus.domain.Reservation;
import mybus.mybus.domain.User;
import mybus.mybus.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/api")
public class ReservationController {
    @Autowired
    ReservationService reservationService;
    @PostMapping("/addReservation")
    public Reservation addReservation(@RequestBody Reservation reservation)
    {


        return reservationService.addReservation(reservation);

    }
}
