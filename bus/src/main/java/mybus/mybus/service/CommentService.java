package mybus.mybus.service;

import mybus.mybus.domain.Comment;
import mybus.mybus.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;
    public  Comment addComment(Comment comment)
    {

       return commentRepository.save(comment);
    }
}
