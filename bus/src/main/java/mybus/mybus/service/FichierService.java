package mybus.mybus.service;

import mybus.mybus.domain.Fichier;
import mybus.mybus.repository.FichierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class FichierService {
    @Autowired
    FichierRepository fichierRepository;
    public Fichier addFile(MultipartFile file) {

        try {
            Fichier fichier = new Fichier(file.getOriginalFilename(), file.getContentType(), file.getBytes());

            return fichierRepository.save(fichier);
        } catch (IOException exc) {
        }
        return null;
    }
}
