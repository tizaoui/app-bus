package mybus.mybus.service;

import mybus.mybus.domain.Fichier;
import mybus.mybus.domain.New;
import mybus.mybus.repository.NewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class NewService {
@Autowired
FichierService fichierService;
@Autowired
    NewRepository newRepository;
    public New addNew(New new1, MultipartFile file)

    {
        if(file!=null)
        {
            Fichier fichier= fichierService.addFile(file);

            new1.setFichier(fichier);}
        return newRepository.save(new1);

    }
    public List<New> getAll()
    {
       return newRepository.findAll();

    }
    public New getById(Integer id)
    {

        return newRepository.findById(id).orElse(null);
    }
}
