package mybus.mybus.service;

import mybus.mybus.domain.Voyage;
import mybus.mybus.repository.VoyageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class VoyageService {
    @Autowired
    VoyageRepository voyageRepository;
    public List<Voyage> findVoyages(LocalDate date,String lieu1,String lieu2)
    {

        return voyageRepository.findByDateAndLieu1AndLieu2(date,lieu1,lieu2);
    }
}
