package mybus.mybus.service;

import mybus.mybus.domain.Reservation;
import mybus.mybus.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {
    @Autowired
    ReservationRepository reservationRepository;
    public Reservation addReservation(Reservation reservation)
    {
        return   reservationRepository.save(reservation);
    }

}
